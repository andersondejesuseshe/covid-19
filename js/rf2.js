window.onload = main;

async function leerJSON(url) {
    try {
        let response = await fetch(url);
        let user = await response.json();
        return user;
    } catch (err) {
        alert(err);
    }
}

function main() {
    let municipios = {};
    leerJSON('https://www.datos.gov.co/resource/gt2j-8ykr.json').then(data =>{
        data.map(x => {
            if (municipios[x.ciudad_municipio_nom]) {
                municipios[x.ciudad_municipio_nom]++;
            } else {
                municipios[x.ciudad_municipio_nom] = 1;
            }
        });
        let html = document.getElementById("div");
        let select = "<select onchange='casosMUN();cargarDatos()' id='MUN'>";
        for (const key in municipios) {
            select += `<option>${key}</option>`;
        }
        select += "</select>"
        console.log(municipios)
        html.innerHTML = select;
    })
}

function casosMUN() {
    let MUN = document.getElementById("MUN").value;
    let estudio = 0;
    let relacionado = 0;
    leerJSON('https://www.datos.gov.co/resource/gt2j-8ykr.json').then(data =>{
        data.map(x => {
            if (x.ciudad_municipio_nom == MUN && x.fuente_tipo_contagio == "En estudio")
                estudio++;
            if (x.ciudad_municipio_nom == MUN && x.fuente_tipo_contagio == "Relacionado")
                relacionado++;
        });
        let table = `
        <table border=1 align="center">
            <thead>
                <th>En estudio</th>
                <th>Relacionado</th>
            </thead>
            <tbody>
            <th>${estudio}</th>
            <th>${relacionado}</th> 
            </tbody>
        `
        document.getElementById("table").innerHTML = table;
        graficar(estudio, relacionado, MUN);
    })
}

function graficar(E, R, MUN) {
    google.charts.load('current', { 'packages': ['corechart'] });
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['tipo contagio', 'Cantidad'],
            ['En estudio', E],
            ['Relacionado', R]
        ]);
        var options = {
            title: `Fuentes de tipos de contagio del municipio ${MUN}`
        };
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
    }
}

